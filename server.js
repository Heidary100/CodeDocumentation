const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

// const dbUrl = "mongodb://user:user123@ds139632.mlab.com:39632/learning-node";

const dbUrl = "mongodb://localhost/CodeDocuments";

const Post = mongoose.model('Post', {
    title: String,
    description: String,
    date: Date
})

app.use(express.static(__dirname));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.set('view engine', 'ejs');

app.get('/add', (req, res) => {
    res.render('add', {message: {}})
});

app.post('/add', (req, res) => {
    let newPost = new Post(req.body);
    newPost.date = new Date();

    console.log(newPost);

    newPost.save((err) => {
        console.log("saved");
        if (!err) {
            res.render('add', {
                message: {
                    success: true,
                    text: "با موفقیت ثبت شد"
                }
            });
        }
        else {
            res.render('add', {
                message: {
                    success: false,
                    text: "با موفقیت ثبت نشد"
                }
            });
        }
    });
});

app.get('/', (req, res) => {
    CodeDocument.find({}, function (err, codeDocuments) {
        res.render('index', {codeDocuments: codeDocuments});
    });
});


mongoose.connect(dbUrl, {useNewUrlParser: true}, (err) => {
    console.log("Unable to connect to the mongodb", err);
})

let server = app.listen(3000, () => {
    console.log("Server is listening on port: " + server.address().port);
});